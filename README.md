Role Name
=========

Тестовое задание на разворачивание Nginx+php-fpm7.2, Galera Cluster MariaDB, NFS?

Description
--------------

Playbook состоит из 3 playbook'ов и 2 задач собранных в одни playbook. По хорошему playbook'и тоже можно переоформить в задачи для упрощения и эстетики. 

Первый и второй PB разворачивают два docker контейнера с nginx и php-fpm:7.2
Третий собирает Galera Cluster 

NFS состоит из 2х задач, серверная и клиентская

Example Playbook
----------------

В playbook'е используется ansible-vault файл называет "secret", в нём описан метод доступа на VM. Перед началом использования его необходимо создать:

в файле hosts у хостов должна быть задана переменная ```host_name``` для определения hostname сервера.

```ansible-vault create secret ```

```
Примерно с таким содержанием:
ansible_user: пользователь
ansible_ssh_pass: пароль
ansible_become_pass: пароль для sudo
```

Запуск:
```ansible-playbook -i hosts play.yml --ask-vault-pass```

